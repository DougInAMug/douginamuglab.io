---
title: Personal projects update
subtitle: Jan 2018
tags: [kanthaus]
layout: remarkjs
description: Update of what I've done in January and plan to do in February.
date: 2018-01-29
weight: 3
draft: false
---

class: center, middle

# What I did in January

---

# Constitution amendment
## + Janina
- major simplification of position systems
- much more human-readable

<img src="/pics/document-properties.png" width="35%">

---

# Anarchapulco application
- wrote script, edited to 8 minutes with Shotcut, uploaded to [YT](https://www.youtube.com/watch?v=eVGB5pQonoE&feature=youtu.be)
- didn't get it
- refined my political opinions

<img src="/pics/anCapBall.jpg" width="35%">

---

# Deutsche
## + many, esp. Arno
- jeder tag besser

<img src="/pics/deFlag.png" width="35%">

---

# Personal website
## + Wolfi
- hugo slowly bends to my will
- Remarkjs slides work

<img src="/pics/recursiveSlide.png" width="75%">

---

# Proposal Party
## + Wolfi
- good attendance
- range of ideas

<img src="/pics/busPropParty.jpg" width="35%">

---

# Other
- read most of Karl Poppers autobiography
- mounted planks in office (+ Tilmann)
- cleared up area around bins (+ Katharina + bro)
- fixed phone
- some work on Kanthaus brand (+ Janina)

---

class: center, middle

# What I want to do in February
(all asterisked tasks* completed → Chinese buffet)

---

# Review commitment
- it's been 6 months now!
- meet with others who are commited*
- decide personally (what and how long)*
- communicate my decision*

<img src="/pics/handshake.png" width="35%">


---

# Gardening
- spring is coming!
- meet with VKers and S&N*
- plan*
- start?

<img src="/pics/peaPlants.jpg" width="35%">


---

# Deutsche
- ein stunde grammatik pro woche (A1 kursbuch und so)*
- deutsche Sitcoms beim Frühstück anschauen
- noch mehr raden

<img src="/pics/deFlag.png" width="35%">

---

# Decision-making documentation*
- review then refine/dispose of all the crap I've written so far
- prepare the most minimal draft which covers—
  - repeat control option
  - semantic range (i.e. support to oppose)
  - acceptance orientation (i.e. multiplication of oppose)
- ukuvota—
  - replace old doc with draft
  - update summary
  - update multiplication explanation

---

# Other
- review Jeroens last draft of 'economic-shifts'
- call with EcoHackerFarm
- catch up with Bonobo e.V.
- see if I can offer HZ anything
- help out with hackweeks
- continue on Kanthaus brand/story work
- travel to Chemnitz
- travel to Leipzig/Dresden
- start thinking about active recruitment
- workover slide css
- find smart way to size images
- ...

