---
title: Personal projects update 3
subtitle: Mar/Apr 2018
tags: [kanthaus]
layout: remarkjs
description: Update of what I've done in February and plan to do in March.
date: 2018-02-20
weight: 3
draft: false
---

class: center, middle

# What I wanted to do in February
(all asterisked tasks* completed → Chinese buffet)

---

# Review commitment
- it's been 6 months now!
- ~~meet with others who are commited*~~
- ~~decide personally (what and how long)*~~
- ~~communicate my decision*~~

<img src="/pics/handshake.png" width="35%">

---

# Gardening
- spring is coming!
- ~~meet with VKers and S&N*~~
- ~~plan*~~
- start?

<img src="/pics/peaPlants.jpg" width="35%">

---

# Deutsche
- ~~ein stunde grammatik pro woche (A1 kursbuch und so)*~~
- deutsche Sitcoms beim Frühstück anschauen
- noch mehr raden

<img src="https://upload.wikimedia.org/wikipedia/commons/c/cb/German_definite_article_declension.png" width="100%">

---

# Other
- ~~review Jeroens last draft of 'economic-shifts'~~
- ~~call with EcoHackerFarm~~ They were happy to talk and interested to cooperate.
- ~~catch up with Bonobo e.V.~~ That verein is dead :(
- ~~see if I can offer HZ anything~~ Steffen will get in touch if needed

---

# Decision-making documentation*
- Context: https://github.com/DougInAMug/
- ~~review then refine/dispose of all the crap I've written so far~~
- ~~prepare the most minimal draft which covers—~~
  - ~~repeat control option~~
  - ~~semantic range (i.e. support to oppose)~~
  - ~~acceptance orientation (i.e. multiplication of oppose)~~
- ~~ukuvota—~~
  - ~~replace old doc with draft~~
  - ~~update summary~~
  - ~~update multiplication explanation~~

---

<img src="https://i.ytimg.com/vi/2sGY4Q_SfPo/maxresdefault.jpg" width="100%">

---

class: center, middle

# What I want to do in March
(not much)

---

# Travel/chill
- Lyon, South coast of France
- Reading, visiting projects

<img src="https://www.nationalgeographic.com/content/dam/science/photos/000/065/6594.ngsversion.1509199314694.adapt.1900.1.jpg
" width="100%">

---

# Finish the Coop Voting stuff
- Integrate feedback
  - not plagiarizing
  - more personal/flowing (less academic)
- Make changes to Ukuvota
- Decide how to publish (and do it)

---

# Think about what next
- brodtka?
- black soldier flies?
- minimal viable coop?
- effective altuism re kanthaus/karrot/foodsharing?
- ...


