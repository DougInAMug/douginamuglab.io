---
title: 2nd Constitutional Amendment
subtitle: proposal by Doug, Janina et al.
tags: [kanthaus]
layout: remarkjs
description: This outlines the second proposal for changing the Kanthaus Constitution. Some smaller changes are suggested to make the English simpler and clearer. Some bigger changes are suggested to unify and clarify the Position system.
date: 2018-01-19
weight: 3
draft: false
---

class: center, middle

# 2nd Constitution Amendment
## Proposal by Doug, Janina _et al._

<img src="/pics/document-properties.png" width="35%">

---

# Full proposal
https://github.com/kanthaus/kanthaus.online/compare/constchange?diff=split&name=constchange

<a href="https://github.com/kanthaus/kanthaus.online/compare/constchange?diff=split&name=constchange
"><img src="/pics/masterPropDiff.png" width="100%"></a>

---

# Smaller changes (1)
Simpler/more descriptive words—
- "prescribe" → "outline" (8)
- "individuals" → "people" (40)
- "application" → "evaluation" (95)
- "Visitor" → "Guest" (39)
- ...

---

# Smaller changes (2)

Simpler/more descriptive phrases—
- "Any addition, subtraction or other change to" → "All changes to" (131)
- "meeting of the actors to bear witness" → "meeting of the involved individuals to witness" (167)
- ...
  
---

# Bigger changes (1)
Addition of Guest position. (39)
- The Guest position has been operational for... 3 months?
- And seems to be working.
- Adding to Constitution unifies Positions.

---

# Bigger changes (2)
Simplification of evaluation periods—
- 'A Guest should be evaluated (§9.) as soon as possible after two weeks (14 days). ...' (49)
- 'A Volunteer should be evaluated (§9.) as soon as possible after 2 months (60 days). ...' (66)
- 'A Member should be evaluated (§9.) as soon as possible after 6 months (180 days). ...' (86)
- Reduces confusion about when to evaluate.
- Also reduces flexibility.

---

# Bigger changes (3)
Addition of informal discussion and feedback to Evaluation Procedure (95)
- We already do these things because they add value.

---

# Bigger changes (4)
Simplification of the phrasing for 'two consecutive rounds' for changing the Constitution (154)

---

# That's all for now folks!
