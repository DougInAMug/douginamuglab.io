---
title: Personal reflections on planning
subtitle: 
tags: [iteration]
description:
date: 2017-01-01
weight: 3
draft: true
---

**Summary**
- Planning enables _action_, not just reaction.
- I've had some bad experiences with planning: too much, too detailed and/or too quickly.
- I've found that incremental planning is best for new projects.
- I try to plan less than I do and at an appropriate level of detail.

I used to make plans for myself which I couldn't achieve: plans which I could have fulfilled in theory, but not in reality. I failed to account for the unexpected, the extra time it takes to do things and all the wonderful, spontaneous things which can't be planned for. The unrealistic expectations I had on myself lead to a constant, underlying feeling of underachievement during my first half of university. 

It really sucked. The vicious cycle wnet something like this—
1. set overly optimistic goals,
2. fail to achieve them,
3. feel bad about it, then
4. motivate myself by planning to do more next time... and back to step 1.

One of the those ideas that dug its way into my mind during childhood came from my dad: _"a man is as good as his word."_ I found it quite hard growing up with this value in contrast to the smartphone/Facebook culture around me, where people (including me) said they were going to attend x, y and z—and then didn't. erhaps it is an invetible consequence of  is good reason to believe that ex

I believe that not being consequent degrades trust; without trust you can't plan for the future and not planning for the future leads to short-term thinking. I believe the short-term perspective is a deep psychological problem for humanity in these times. To quote Shevek, the protagonist from the (incredible) sci-fi novel _The Disposessed_ by Ursula Le Guin: _"To break a promise is to deny the reality of the past; therefore it is to deny the hope of a real future."_

Back to uni: one day, I was chatting with a friend during my third year and we got onto this subject of planning. He said _"set achievable goals and do them."_ Amazing, so simple! Set goals so low that you will definitely be able to do them and then slowly build up! So instead of planning to do a 20 minute cardio session 3 times per week and failing, I would plan to do 1 press-up 3 times per week and succeed. The thing is that I never did just 1 press-up, I would always do more than that, but I managed to be consequent with myself and that was really awesome.

Being introduced to [Agile](http://agilemanifesto.org/), [Scrum](http://scrumguides.org/scrum-guide.html) and that whole incremental software development scene blew my mind. The idea of planning at the right detail made a lot of sense, for example: a big/far away goal is more uncertain than the top priority task of the moment, as such the goal should be planned more vaguely and the task in more detail. Reading a [short book about Karl Popper](https://www.docdroid.net/KflQzSx/philosophy-and-the-real-world-an-introduction-to-karl-popper-bryan-magee.pdf#page=7) and his philosophy of science, logic and society was also really illuminating, especially since his ideas are the bedrock of the current scientific method upon which Scrum and friends are based.

<img src="https://static1.squarespace.com/static/511269bbe4b0c73df72dc118/t/59a714d6e3df28f2da211186/1504122071983/waterfall-and-agile-methods.jpg" alt="comparison of waterfall and iterative" style="margin:0px auto;display:block" width="650">

I have had some bad experiences planning with others. Having my time and effort spent on preparation wasted when others have not kept to a plan is very frustrating for me. I have done it to others as well, and I really hate it. Generally when I plan to do something with others, I personally commit to putting the fulfillment of the plan above how I might feel at the time of execution: perhaps I might be a little bit tired, but I'll still go to a meeting if I solidly agreed upon it. This doesn't mean I consider every plan to be an unalterable contract, but I do take them seriously. 

Now with I start with low expectations of planning with others, building them over time if justified. Sometimes this can be as simple as understanding which language people use to mean what. When I'm with people who try to define very big/far away goals in fine detail with high commitment, I have a strong negative reaction. I think that big/far away goals are really important, but that they should be defined only in rough detail to reflect the increasing uncertainty of the future. This allows people to integrate new information and to be creative and adaptive regarding the details instead of blindly following The Plan™ or breaking it.

<img src="https://agilewarrior.files.wordpress.com/2013/04/cone-of-uncertainty.png" alt="cone of uncertainty" style="margin:0px auto;display:block" width="500">

I've come a long way, but I've got a long way to go. For myself, I really want to plan more things that I find fun, because I naturally gravitate towards tasks even if I don't do them effectively. Being accountable to others about what I plan for myself is really helpful! I would like to find a way for people to celebrate their individual achievements together.

When working with others I generally want to be clearer about planning—that doesn't necessarily mean more detailed! A broad goal can still be clear even if not detailed. I find things get done much better when precise planning details are recorded: what is the minimal task description, who exactly has committed to it, when does it need to be done, etc. Having regular reflection and planning sessions is also great.
